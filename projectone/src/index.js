import React from "react";
import ReactDOM from 'react-dom'

const App = function () {
    return (
        <div style = {{
            display: 'flex'
        }}>
            <Months></Months>
            <Week></Week>
            <Zodiac></Zodiac>
       </div>
    )
}

const Week = function () {
    return (
        <div style = {{
            padding: '10px'
        }}>
            <h1>Дні тижня</h1>
            <ul>
                <li>Понеділок</li>
                <li>Вівторок</li>
                <li>Середа</li>
                <li>Четверг</li>
                <li>П'ятниця</li>
                <li>Субота</li>
                <li>Неділя</li>
            </ul>
        </div>
    )
}

const Months = function () {
    return (
        <div style = {{
            padding: '10px'
        }}>
            <h1>Місяць</h1>
            <ul>
                <li>Січень</li>
                <li>Лютий</li>
                <li>Березень</li>
                <li>Квітень</li>
                <li>Травень</li>
                <li>Червень</li>
                <li>Липень</li>
                <li>Серпень</li>
                <li>Вересень</li>
                <li>Жовтень</li>
                <li>Листопад</li>
                <li>Грудень</li>
            </ul>
        </div>
    )
}

const Zodiac = function () {
    return (
        <div style = {{
            padding: '10px'
        }}>
            <h1>Знаки зодіаку</h1>
            <ul>
                <li>Козеріг</li>
                <li>Водолій</li>
                <li>Риби</li>
                <li>Овен</li>
                <li>Телець</li>
                <li>Близнюки</li>
                <li>Рак</li>
                <li>Лев</li>
                <li>Діва</li>
                <li>Терези</li>
                <li>Скорпіон</li>
                <li>Стрілець</li>
            </ul>
        </div>
    )
}

ReactDOM.render(<App></App> , document.querySelector('#root'))